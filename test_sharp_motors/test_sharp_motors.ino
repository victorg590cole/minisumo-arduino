#include <AFMotor.h>
#include <Servo.h>

// Si es defineix, el programa és "Robot netejador". Sinó, "Gos que vigila"
#define LLAUNES 3

const int sharp = A1;
AF_DCMotor motorD(1);
AF_DCMotor motorE(2);
Servo servo;

const int dist = 275;
// Velocitat més lenta:
unsigned char motorSpeed = 250;
#if LLAUNES
const int cnyD = A4;
const int cnyE = A3; // AD
const int cnyMax = 300;
int llaunesFora;
#endif

void fi();

void setup() {
  motorD.setSpeed(motorSpeed);
  motorE.setSpeed(motorSpeed);
  motorD.run(RELEASE);
  motorE.run(RELEASE);
  servo.attach(10);
  #if LLAUNES
  llaunesFora = 0;
  #endif
}

void loop() {
  servo.write(175); // Rampa baixa
  if (analogRead(sharp) >= dist) {
    motorD.run(FORWARD);
    motorE.run(FORWARD);
  } else {
    motorD.run(RELEASE);
    motorE.run(RELEASE);
  }
  #if LLAUNES
  if ((analogRead(cnyD) <= 300) || (analogRead(cnyE) <= 300)) {
    llaunesFora++;
    if (llaunesFora >= LLAUNES) fi();
  }
  #endif
}

void fi() {
  motorD.run(FORWARD);
  motorE.run(BACKWARD);
  delay(2000);
  motorD.run(RELEASE);
  motorE.run(RELEASE);
  do {} while (true); // ACABA
}

