#undef DIGITAL

const int cnyPin = A4;
const int cnyAdPin = A3;

bool cnyRead(int pin) {
#ifdef DIGITAL
  return digitalRead(pin) == HIGH;
#else
  if (analogRead(pin) <= 300)
    return false;
  else
    return true;
#endif
}

void setup() {
  Serial.begin(9600);
#ifdef DIGITAL
  pinMode(cnyPin, INPUT);
  pinMode(cnyAdPin, INPUT);
#endif
}

void loop() {
  Serial.print("CNY N: ");
  Serial.print(cnyRead(cnyPin));
#ifndef DIGITAL
  Serial.print(" (");
  Serial.print(analogRead(cnyPin));
  Serial.println(")");
#endif
  Serial.print("CNY AD: ");
  Serial.print(cnyRead(cnyAdPin));
#ifndef DIGITAL
  Serial.print(" (");
  Serial.print(analogRead(cnyAdPin));
  Serial.println(")");
#endif
  Serial.println();
  delay(500);
}
