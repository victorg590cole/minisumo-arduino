import processing.serial.*;

PImage diseno;

static Serial serial;
static boolean isSerial;

void setup() {
  size(550, 550);
  background(#FFFFFF);
  diseno = loadImage ("flechas.png");
  image(diseno, 0, 0);
  printArray(Serial.list());
  try {
    serial = new Serial(this, Serial.list()[0], 9600);
    isSerial = true;
  } 
  catch (ArrayIndexOutOfBoundsException ex) {
    isSerial = false;
  }
}

void draw() {

  //forward
  //stroke(#00D7FF);
  noStroke();
  strokeWeight(1);
  noFill();
  rectMode(RADIUS);
  rect(250, 70, 80, 50);

  //back
  rect(250, 450, 80, 50);

  //left
  rect(70, 250, 50, 80);

  //right
  rect(450, 250, 50, 80);

  //automode
  rect(435, 435, 60, 60);

  //stop
  rect(257, 257, 30, 30);

  //fastturnright
  rect(338, 257, 30, 60);

  //fastturnleft
  rect(176, 257, 30, 60);

  //rampup
  rect(257, 180, 50, 38);

  //rampdown
  rect(257, 340, 50, 38);
}

void keyPressed() {
  switch (key) {
  case 'w':
  case 'W':
    Accions.atacar();
    break;
  case 'a':
  case 'A':
    Accions.girar();
    break;
  case 'q':
  case 'Q':
    Accions.girarR();
    break;
  case 's':
  case 'S':
    Accions.enrere();
    break;
  case 'd':
  case 'D':
    Accions.girarI();
    break;
  case 'e':
  case 'E':
    Accions.girarIR();
    break;
  case 'r':
  case 'R':
    Accions.pujarRampa();
    break;
  case 'f':
  case 'F':
    Accions.baixarRampa();
    break;
  case ' ':
    Accions.aturar();
    break;
  case ENTER:
  case RETURN:
    Accions.automatic();
    break;
  }
}

void mousePressed() {
  if (mouseX < 330 && mouseX > 170 && mouseY < 120 && mouseY > 30)
    Accions.atacar();
  else if (mouseX < 330 && mouseX > 170 && mouseY < 500 && mouseY > 400)
    Accions.enrere();
    else if (mouseX < 130 && mouseX > 20 && mouseY < 330 && mouseY > 170)
    Accions.girar();
    else if (mouseX < 500 && mouseX > 400 && mouseY < 330 && mouseY > 170)
    Accions.girarI();
    else if (mouseX < 206 && mouseX > 146 && mouseY < 317 && mouseY > 197)
    Accions.girarR();
    else if (mouseX < 368 && mouseX > 308 && mouseY < 317 && mouseY > 197)
    Accions.girarIR();
    else if (mouseX < 307 && mouseX > 207 && mouseY < 218 && mouseY > 142)
    Accions.pujarRampa();
    else if (mouseX < 307 && mouseX > 207 && mouseY < 378 && mouseY > 302)
    Accions.baixarRampa();
    else if (mouseX < 290 && mouseX > 224 && mouseY < 290 && mouseY > 224)
    Accions.aturar();
    else if (mouseX < 495 && mouseX > 375 && mouseY < 495 && mouseY > 375)
    Accions.automatic();
}    

private static class Accions {
  static void atacar() {
    if (isSerial)
      serial.write(AccionsSerial.ATACAR.val);
    else
      println("ATACAR");
  }

  static void enrere() {
    if (isSerial)
      serial.write(AccionsSerial.ENRERE.val);
    else
      println("ENRERE");
  }

  static void girar() {
    if (isSerial)
      serial.write(AccionsSerial.GIRAR.val);
    else
      println("GIRAR");
  }

  static void girarR() {
    if (isSerial)
      serial.write(AccionsSerial.GIRAR_R.val);
    else
      println("GIRAR (R)");
  }

  static void girarI() {
    if (isSerial)
      serial.write(AccionsSerial.GIRAR_I.val);
    else
      println("GIRAR (I)");
  }

  static void girarIR() {
    if (isSerial)
      serial.write(AccionsSerial.GIRAR_IR.val);
    else
      println("GIRAR (IR)");
  }

  static void aturar() {
    if (isSerial)
      serial.write(AccionsSerial.ATURAR.val);
    else
      println("ATURAR");
  }

  static void pujarRampa() {
    if (isSerial)
      serial.write(AccionsSerial.PUJAR_RAMPA.val);
    else
      println("PUJAR RAMPA");
  }

  static void baixarRampa() {
    if (isSerial)
      serial.write(AccionsSerial.BAIXAR_RAMPA.val);
    else
      println("BAIXAR RAMPA");
  }

  static void automatic() {
    if (isSerial)
      serial.write(AccionsSerial.AUTOMATIC.val);
    else
      println("AUTOMÀTIC");
  }
}

/* Un enum amb les accions reconegudes.
 * Ha de coincidir amb les de l'Arduino
 */
private static enum AccionsSerial {
  ATURAR(1), 
    ATACAR(2), 
    ENRERE(3), 
    GIRAR(4), 
    GIRAR_R(5), 
    GIRAR_I(6), 
    GIRAR_IR(7), 
    PUJAR_RAMPA(8), 
    BAIXAR_RAMPA(9), 
    AUTOMATIC(10);

  final int val; // El valor (int) de l'acció
  AccionsSerial(int a) {
    this.val = a;
  }
}