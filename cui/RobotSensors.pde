public static class RobotSensors {
  private JSONObject sensorData;

  private boolean digitalCnys;
  private static final boolean DIGITAL_CNYS_DEF = false;

  private int sharp;
  private static final int SHARP_DEF = -1;

  private int cnyDAnalog;
  private static final int CNY_D_ANALOG_DEF = -1;

  private boolean cnyDDigital;
  private static final boolean CNY_D_DIGITAL_DEF = false;

  private int cnyEAnalog;
  private static final int CNY_E_ANALOG_DEF = -1;

  private boolean cnyEDigital;
  private static final boolean CNY_E_DIGITAL_DEF = false;

  private boolean blanc;
  private static final boolean BLANC_DEF = false;

  private AccionsSerial accio;
  private static final AccionsSerial ACCIO_DEF = AccionsSerial.ATURAR;

  private int motorDSpeed;
  private static final int MOTOR_D_SPEED_DEF = 0;

  private String motorDSentit;
  private static final String MOTOR_D_SENTIT_DEF = "";

  private int motorESpeed;
  private static final int MOTOR_E_SPEED_DEF = 0;

  private String motorESentit;
  private static final String MOTOR_E_SENTIT_DEF = "";

  private boolean servo;
  private static final boolean SERVO_DEF = false;

  public RobotSensors() {
    sensorData = null;
    sharp = SHARP_DEF;
    digitalCnys = DIGITAL_CNYS_DEF;
    cnyDAnalog = CNY_D_ANALOG_DEF;
    cnyDDigital = CNY_D_DIGITAL_DEF;
    cnyEAnalog = CNY_E_ANALOG_DEF;
    cnyEDigital = CNY_E_DIGITAL_DEF;
    blanc = BLANC_DEF;
    accio = ACCIO_DEF;
    motorDSpeed = MOTOR_D_SPEED_DEF;
    motorDSentit = MOTOR_D_SENTIT_DEF;
    motorESpeed = MOTOR_E_SPEED_DEF;
    motorESentit = MOTOR_E_SENTIT_DEF;
    servo = SERVO_DEF;
  }

  public String getSharp() {
    return String.valueOf(sharp);
  }

  public String getCnyD() {
    if (digitalCnys) {
      if (cnyDDigital)
        return "HIGH";
      else
        return "LOW";
    } else
      return String.valueOf(cnyDAnalog);
  }

  public String getCnyE() {
    if (digitalCnys) {
      if (cnyEDigital)
        return "HIGH";
      else
        return "LOW";
    } else
      return String.valueOf(cnyEAnalog);
  }

  public String isBlanc() {
    if (blanc)
      return "Blanc";
    else
      return "Negre";
  }

  public String getAccio() {
    return accio.desc;
  }

  public String getMotorDSpeed() {
    return String.valueOf(motorDSpeed);
  }

  public String getMotorDSentit() {
    if (motorDSentit.equalsIgnoreCase("FORWARD"))
      return "FORWARD";
    else if (motorDSentit.equalsIgnoreCase("BACKWARD"))
      return "BACKWARD";
    else if (motorDSentit.equalsIgnoreCase("RELEASE"))
      return "RELEASE";
    else
      return "N/A";
  }

  public String getMotorESpeed() {
    return String.valueOf(motorESpeed);
  }

  public String getMotorESentit() {
    if (motorESentit.equalsIgnoreCase("FORWARD"))
      return "FORWARD";
    else if (motorESentit.equalsIgnoreCase("BACKWARD"))
      return "BACKWARD";
    else if (motorESentit.equalsIgnoreCase("RELEASE"))
      return "RELEASE";
    else
      return "N/A";
  }

  public String getRampa() {
    if (servo)
      return "Alta";
    else
      return "Baixa";
  }

  public void parse(String json) {
    sensorData = JSONObject.parse(json);
    println(json);
    digitalCnys = sensorData.getBoolean("digitalCnys", DIGITAL_CNYS_DEF);
    sharp = sensorData.getInt("sharp", SHARP_DEF);
    if (digitalCnys) {
      cnyDAnalog = CNY_D_ANALOG_DEF;
      cnyEAnalog = CNY_E_ANALOG_DEF;
      cnyDDigital = sensorData.getJSONArray("cnyD").getBoolean(1, CNY_D_DIGITAL_DEF);
      cnyEDigital = sensorData.getJSONArray("cnyE").getBoolean(1, CNY_E_DIGITAL_DEF);
    } else {
      cnyDAnalog = sensorData.getJSONArray("cnyD").getInt(0, CNY_D_ANALOG_DEF);
      cnyEAnalog = sensorData.getJSONArray("cnyE").getInt(0, CNY_E_ANALOG_DEF);
      cnyDDigital = CNY_D_DIGITAL_DEF;
      cnyEDigital = CNY_E_DIGITAL_DEF;
    }
    blanc = sensorData.getBoolean("blanc", BLANC_DEF);
    accio = AccionsSerial.parse(sensorData.getInt("accio", 0));
    if (accio == null)
      accio = ACCIO_DEF;
    motorDSpeed = sensorData.getJSONObject("motorD").getInt("speed", MOTOR_D_SPEED_DEF);
    motorDSentit = sensorData.getJSONObject("motorD").getString("sentit", MOTOR_D_SENTIT_DEF);
    motorESpeed = sensorData.getJSONObject("motorE").getInt("speed", MOTOR_E_SPEED_DEF);
    motorESentit = sensorData.getJSONObject("motorE").getString("sentit", MOTOR_E_SENTIT_DEF);
    servo = sensorData.getBoolean("servo", SERVO_DEF);
  }
}