import processing.serial.*;

int xInicial = 15;
int yInicial = 15;
int xRect = 175;
int yRect = 50;
int rectMarge = 10;
int corner = 0;

RobotSensors sensors;

static Serial serial;
static boolean isSerial;

void setup() {
  size(750, 550);
  printArray(Serial.list());
  try {
    serial = new Serial(this, Serial.list()[0], 9600);
    isSerial = true;
    serial.bufferUntil('\n');
  } 
  catch (ArrayIndexOutOfBoundsException ex) {
    // Si no es troba cap port sèrie, es mostraran les accions a la consola,
    // en lloc de enviar-les a l'Arduino.
    isSerial = false;
  }
  sensors = new RobotSensors();
}

// Fa un rectangle a la (X, Y) (coordenades de fila i columna, no de Processing)
private void mkRect(int x, int y) {
  rect(xInicial + (xRect + rectMarge) * x, yInicial + (yRect + rectMarge) * y, xRect, yRect, corner);
}

// Crea un text a (X, Y) (coordenades de fila i columna, no de Processing)
private void mkText(String txt, int x, int y) {
  text(txt, ((xInicial + (xRect + rectMarge) * x) * 2 + xRect) / 2, ((yInicial + (yRect + rectMarge) * y) * 2 + yRect) / 2);
}

// Comprova si el mouse és al rectangle (X, Y)
private boolean onRect(int x, int y) {
  return (mouseX > (xInicial + (xRect + rectMarge) * x) && mouseX < (xInicial + (xRect + rectMarge) * x) + xRect
    && mouseY > (yInicial + (yRect + rectMarge) * y) && mouseY < ((yInicial + (yRect + rectMarge) * y) + yRect));
}

void draw() {
  background(#FFFFFF);
  PFont f = loadFont("Consolas-48.vlw");
  textSize(16);
  textAlign(LEFT, TOP);
  text("Axel Sanz i Victor Guardia, 1er Batx. A", 0, 0);
  // Rectangles
  rectMode(CORNER);
  fill(#00FF00);
  mkRect(1, 0); // ATACA
  mkRect(1, 1); // ENRERE
  fill(#3827FF);
  mkRect(0, 1); // GIR
  mkRect(2, 1); // GIRA (I)
  mkRect(0, 2); // GIRA (R)
  mkRect(2, 2); // GIRA (RI)
  fill(#FF0000);
  mkRect(1, 2); // ATURA
  fill(#B8B2FF);
  mkRect(3, 1); // PUJAR RAMPA
  mkRect(3, 2); // BAIXAR RAMPA
  fill(#00FF00);
  mkRect(1, 3); // AUTOMÀTIC
  fill(#B8B2FF);
  mkRect(0, 3); // LEDS ON
  mkRect(2, 3); // LEDS OFF

  // Text
  textFont(f);
  textSize(20);
  textAlign(CENTER, CENTER);
  fill(#000000);
  mkText("ATACA (w)", 1, 0);
  mkText("ENRERE (s)", 1, 1);
  fill(#FFFFFF);
  mkText("GIRA (a)", 0, 1);
  mkText("GIR INVERS (d)", 2, 1);
  mkText("GIR RÀPID (A)", 0, 2);
  mkText("GIR I. R. (D)", 2, 2);
  mkText("ATURA (espai)", 1, 2);
  fill(#000000);
  mkText("PUJAR RAMPA (q)", 3, 1);
  mkText("BAIXAR RAMPA (e)", 3, 2);
  mkText("AUTOMÀTIC (x)", 1, 3);
  mkText("LEDS ON (z)", 0, 3);
  mkText("LEDS OFF (c)", 2, 3);

  textSize(18);
  mkText("Sharp", 0, 4);
  mkText(sensors.getSharp(), 1, 4);
  mkText("Rampa", 2, 4);
  mkText(sensors.getRampa(), 3, 4);
  mkText("Acció", 0, 5);
  mkText(sensors.getAccio(), 1, 5);
  mkText("Blanc?", 2, 5);
  mkText(sensors.isBlanc(), 3, 5);
  mkText("CNY dret", 0, 6);
  mkText(sensors.getCnyD(), 1, 6);
  mkText("CNY esquerre", 2, 6);
  mkText(sensors.getCnyE(), 3, 6);
  mkText("Motor dret", 0, 7);
  mkText(sensors.getMotorDSentit(), 1, 7);
  mkText("Velocitat", 2, 7);
  mkText(sensors.getMotorDSpeed(), 3, 7);
  mkText("Motor esquerre", 0, 8);
  mkText(sensors.getMotorESentit(), 1, 8);
  mkText("Velocitat", 2, 8);
  mkText(sensors.getMotorESpeed(), 3, 8);
}

void keyPressed() {
  switch (key) {
  case 'W':
  case 'w':
  case '8':
    Accions.atacar();
    break;
  case 'S':
  case 's':
  case '2':
    Accions.enrere();
    break;
  case 'a':
  case '4':
    Accions.girar();
    break;
  case 'A':
    Accions.girarR();
    break;
  case 'd':
  case '6':
    Accions.girarI();
    break;
  case 'D':
    Accions.girarIR();
    break;
  case '0':
  case ' ':
    Accions.aturar();
    break;
  case 'q':
  case 'Q':
    Accions.pujarRampa();
    break;
  case 'e':
  case 'E':
    Accions.baixarRampa();
    break;
  case 'x':
  case 'X':
    Accions.automatic();
    break;
  case 'z':
  case 'Z':
    Accions.ledsOn();
    break;
  case 'c':
  case 'C':
    Accions.ledsOff();
    break;
  case CODED:
    switch (keyCode) {
    case UP:
      Accions.atacar();
      break;
    case DOWN:
      Accions.enrere();
      break;
    case LEFT:
      Accions.girar();
      break;
    case RIGHT:
      Accions.girarI();
      break;
    }
  }
}

void mousePressed() {
  if (onRect(1, 0))
    Accions.atacar();
  else if (onRect(1, 1))
    Accions.enrere();
  else if (onRect(0, 1))
    Accions.girar();
  else if (onRect(2, 1))
    Accions.girarI();
  else if (onRect(0, 2))
    Accions.girarR();
  else if (onRect(2, 2))
    Accions.girarIR();
  else if (onRect(1, 2))
    Accions.aturar();
  else if (onRect(3, 1))
    Accions.pujarRampa();
  else if (onRect(3, 2))
    Accions.baixarRampa();
  else if (onRect(1, 3))
    Accions.automatic();
  else if (onRect(0, 3))
    Accions.ledsOn();
  else if (onRect(2, 3))
    Accions.ledsOff();
}

void serialEvent(Serial s) {
  sensors.parse(s.readStringUntil('\n'));
  redraw();
}

private static class Accions {
  static void atacar() {
    if (isSerial)
      serial.write(AccionsSerial.ATACAR.val);
    else
      println("ATACAR");
  }

  static void enrere() {
    if (isSerial)
      serial.write(AccionsSerial.ENRERE.val);
    else
      println("ENRERE");
  }

  static void girar() {
    if (isSerial)
      serial.write(AccionsSerial.GIRAR.val);
    else
      println("GIRAR");
  }

  static void girarR() {
    if (isSerial)
      serial.write(AccionsSerial.GIRAR_R.val);
    else
      println("GIRAR (R)");
  }

  static void girarI() {
    if (isSerial)
      serial.write(AccionsSerial.GIRAR_I.val);
    else
      println("GIRAR (I)");
  }

  static void girarIR() {
    if (isSerial)
      serial.write(AccionsSerial.GIRAR_IR.val);
    else
      println("GIRAR (IR)");
  }

  static void aturar() {
    if (isSerial)
      serial.write(AccionsSerial.ATURAR.val);
    else
      println("ATURAR");
  }

  static void pujarRampa() {
    if (isSerial)
      serial.write(AccionsSerial.PUJAR_RAMPA.val);
    else
      println("PUJAR RAMPA");
  }

  static void baixarRampa() {
    if (isSerial)
      serial.write(AccionsSerial.BAIXAR_RAMPA.val);
    else
      println("BAIXAR RAMPA");
  }

  static void automatic() {
    if (isSerial)
      serial.write(AccionsSerial.AUTOMATIC.val);
    else
      println("AUTOMÀTIC");
  }

  static void ledsOn() {
    if (isSerial)
      serial.write(AccionsSerial.LEDS_ON.val);
    else
      println("LEDS ON");
  }

  static void ledsOff() {
    if (isSerial)
      serial.write(AccionsSerial.LEDS_OFF.val);
    else
      println("LEDS OFF");
  }

  /*static void ledsAuto() {
   if (isSerial)
   serial.write(AccionsSerial.LEDS_AUTO.val);
   else
   println("LEDS AUTO");
   }*/  // No utilitzat
}
/* Un enum amb les accions reconegudes.
 * Ha de coincidir amb les de l'Arduino
 */
private static enum AccionsSerial {
  ATURAR(1, "Aturar"), 
    ATACAR(2, "Atacar"), 
    ENRERE(3, "Enrere"), 
    GIRAR(4, "Girar"), 
    GIRAR_R(5, "Girar (R)"), 
    GIRAR_I(6, "Girar (I)"), 
    GIRAR_IR(7, "Girar (IR)"), 
    PUJAR_RAMPA(8, ""), // No s'hauria de mostrar 
    BAIXAR_RAMPA(9, ""), 
    AUTOMATIC(10, "Mode automàtic"), 
    LEDS_ON(11, ""), 
    LEDS_OFF(12, ""), 
    LEDS_AUTO(13, "");

  final int val; // El valor (int) de l'acció
  final String desc;
  AccionsSerial(int val, String desc) {
    this.val = val;
    this.desc = desc;
  }

  public static AccionsSerial parse(int n) {
    switch (n) {
    case 1:
      return ATURAR;
    case 2:
      return ATACAR;
    case 3:
      return ENRERE;
    case 4:
      return GIRAR;
    case 5:
      return GIRAR_R;
    case 6:
      return GIRAR_I;
    case 7:
      return GIRAR_IR;
    case 8:
      return PUJAR_RAMPA;
    case 9:
      return BAIXAR_RAMPA;
    case 10:
      return AUTOMATIC;
    case 11:
      return LEDS_ON;
    case 12:
      return LEDS_OFF;
    case 13:
      return LEDS_AUTO;
    default:
      return null;
    }
  }
}