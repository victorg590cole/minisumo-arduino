#pragma once

#include <Arduino.h>
#include <AFMotor.h>
#include <Servo.h>
#include <ArduinoJson.h>
#include "config.hpp"

typedef enum {
  ATURAR = 1,
  ATACAR = 2,
  ENRERE = 3,
  GIRAR = 4,
  GIRAR_R = 5,
  GIRAR_I = 6,
  GIRAR_IR = 7,
  PUJAR_RAMPA = 8,
  BAIXAR_RAMPA = 9,
  AUTOMATIC = 10,
  LEDS_ON = 11,
  LEDS_OFF = 12,
  LEDS_AUTO = 13
} AccionsSerial;

void _motorSpeed(AF_DCMotor*, AF_DCMotor*, unsigned char);
void _atacar(AF_DCMotor*, AF_DCMotor*);
void _aturar(AF_DCMotor*, AF_DCMotor*);
void _enrere(AF_DCMotor*, AF_DCMotor*);
void _girar(AF_DCMotor*, AF_DCMotor*, bool, bool);
void _chLeds(int, bool);
void _setServo(Servo*, int);
bool _delayControlat(int, int, int);
bool _delayControlat(int, int, int, int);
bool _llegirCnys(int, int);
#ifdef SERIAL_CONTROL
void sendInfoData(int sharpVal, int cnyDPin, int cnyEPin, int accio,
                  unsigned char speedMotorD, int sentitMotorD,
                  unsigned char speedMotorE, int sentitMotorE,
                  bool servo); // Sense macro
#endif

extern int estatMotorD;
extern int estatMotorE;

#define motorSpeed(s) _motorSpeed(&motorD, &motorE, s)
#define atacar() _atacar(&motorD, &motorE)
#define aturar() _aturar(&motorD, &motorE)
#define enrere() _enrere(&motorD, &motorE)
#define girar(r, i) _girar(&motorD, &motorE, r, i)
#define chLeds() _chLeds(leds, ledsOn)
#define delayControlat(t) _delayControlat(cnyD, cnyE, t)
#define delayControlatS(t, s) _delayControlat(cnyD, cnyE, t, s)
#define setServo(p) _setServo(&servo, p)
#define llegirCnys() _llegirCnys(cnyD, cnyE)
