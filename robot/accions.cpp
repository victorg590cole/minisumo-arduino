#include "accions.hpp"

void _motorSpeed(AF_DCMotor* motorD, AF_DCMotor* motorE,
                 unsigned char s) {
  motorD->setSpeed(s);
  motorE->setSpeed(s);
}

void _atacar(AF_DCMotor* motorD, AF_DCMotor* motorE) {
  motorD->run(FORWARD);
  estatMotorD = FORWARD;
  motorE->run(FORWARD);
  estatMotorE = FORWARD;
}

void _aturar(AF_DCMotor* motorD, AF_DCMotor* motorE) {
  motorD->run(RELEASE);
  estatMotorD = RELEASE;
  motorE->run(RELEASE);
  estatMotorE = RELEASE;
}

void _enrere(AF_DCMotor* motorD, AF_DCMotor* motorE) {
  motorD->run(BACKWARD);
  estatMotorD = BACKWARD;
  motorE->run(BACKWARD);
  estatMotorE = BACKWARD;
}

void _girar(AF_DCMotor* motorD, AF_DCMotor* motorE, bool rapid, bool invers) {
  if (invers) {
    motorE->run(FORWARD);
    estatMotorE = FORWARD;
    if (rapid) {
      motorD->run(BACKWARD);
      estatMotorD = BACKWARD;
    } else {
      motorD->run(RELEASE);
      estatMotorD = RELEASE;
    }
  } else {
    motorD->run(FORWARD);
    estatMotorD = FORWARD;
    if (rapid) {
      motorE->run(BACKWARD);
      estatMotorE = BACKWARD;
    } else {
      motorE->run(RELEASE);
      estatMotorE = RELEASE;
    }
  }
}

void _chLeds(int leds, bool ledsOn) {
  digitalWrite(leds, ledsOn ? HIGH : LOW);
}

void _setServo(Servo* servo, int pos) {
  servo->write(pos);
}

bool _delayControlat(int cnyD, int cnyE, int t) {
  return _delayControlat(cnyD, cnyE, t, 1);
}

bool _delayControlat(int cnyD, int cnyE, int t, int s) {
  for (int i = 1; i <= t; i += s) {
    delay(s);
    if (_llegirCnys(cnyD, cnyE))
      return false;
  }
  return true;
}

bool _llegirCnys(int cnyD, int cnyE) {
#ifdef CNY_MAX
  return (analogRead(cnyD) <= CNY_MAX || analogRead(cnyE) <= CNY_MAX);
#else
  return (digitalRead(cnyD) == LOW || digitalRead(cnyD) == LOW);
#endif
}

#ifdef SERIAL_CONTROL
void sendInfoData(int sharpVal, int cnyDPin, int cnyEPin, int accio,
                   unsigned char speedMotorD, int sentitMotorD, unsigned char speedMotorE, int sentitMotorE,
                   bool servo) {
  StaticJsonBuffer<500> jsonBuffer;

  JsonObject& data = jsonBuffer.createObject();
#ifdef CNY_MAX
  data["digitalCnys"] = false;
  JsonArray& cnyD = data.createNestedArray("cnyD");
  cnyD.add(analogRead(cnyDPin));
  cnyD.add(false);
  JsonArray& cnyE = data.createNestedArray("cnyE");
  cnyE.add(analogRead(cnyEPin));
  cnyE.add(false);
#else
  data["digitalCnys"] = true;
  JsonArray& cnyD = data.createNestedArray("cnyD");
  cnyD.add(0);
  cnyD.add(digitalRead(cnyEPin) == LOW);
  JsonArray& cnyE = data.createNestedArray("cnyE");
  cnyE.add(0);
  cnyE.add(digitalRead(cnyEPin) == LOW);
#endif
  data["sharp"] = sharpVal;
  data["blanc"] = _llegirCnys(cnyDPin, cnyEPin);
  data["accio"] = accio;
  JsonObject& motorD = data.createNestedObject("motorD");
  motorD["speed"] = speedMotorD;
  switch (sentitMotorD) {
    case FORWARD:
      motorD["sentit"] = "FORWARD";
      break;
    case BACKWARD:
      motorD["sentit"] = "BACKWARD";
      break;
    case RELEASE:
      motorD["sentit"] = "RELEASE";
      break;
  }
  JsonObject& motorE = data.createNestedObject("motorE");
  motorE["speed"] = speedMotorE;
  switch (sentitMotorE) {
    case FORWARD:
      motorE["sentit"] = "FORWARD";
      break;
    case BACKWARD:
      motorE["sentit"] = "BACKWARD";
      break;
    case RELEASE:
      motorE["sentit"] = "RELEASE";
      break;
  }
  data["servo"] = servo;
  data.printTo(Serial);
  Serial.println();
}
#endif
