#include "accions.hpp"

const int cnyD = A4;
const int cnyE = A3; // AD
const int sharp = A1;
const int leds = 8;
AF_DCMotor motorD(1);
AF_DCMotor motorE(2);
Servo servo;

bool automatic;
bool iniciat;
bool ledsOn;

int rampaAlta = 70;
int rampaBaixa = 158;

int mSpeed = 253;

AccionsSerial accio;

bool rampaPujada;

int estatMotorD;
int estatMotorE;

inline bool distAtaca(int dist) {
  return dist > 150;
}

inline bool distRampa(int dist) {
  return dist >= 350;
}

void setup() {
  motorSpeed(mSpeed);
  pinMode(leds, OUTPUT);
  ledsOn = false;
  chLeds();
  servo.attach(10);
  setServo(rampaBaixa);
  rampaPujada = false;
  automatic = AUTO_DEFAULT;
#ifndef CNY_MAX // Si els CNYs estan com analògics (CNY_MAX no està definit)
  pinMode(cnyD, INPUT);
  pinMode(cnyE, INPUT);
#endif
  Serial.begin(9600);
  while (!Serial) {} // Esperar fins que s'hagi iniciat Serial
  if (automatic)
    accio = AUTOMATIC;
  else
    accio = ATURAR;
  estatMotorD = RELEASE;
  estatMotorE = RELEASE;
  iniciat = false;
}

void loop() {
  if (automatic) { // Mode automàtic
    setServo(rampaBaixa);
    if (!iniciat) {
      ledsOn = true;
      chLeds();
      delay(5000);
      ledsOn = false;
      chLeds();
      iniciat = true;
    }
    int distLlegida = analogRead(sharp);
    if (llegirCnys()) {
      enrere();
      delay(1000);
      girar(true, false);
      delay(1000);
      aturar();
    } else if (distRampa(distLlegida)) {
      setServo(rampaAlta);
      atacar();
      delayControlat(1000);
      aturar();
      setServo(rampaBaixa);
      delay(500); // Temps per a que baixi la rampa
    } else if (distAtaca(distLlegida)) {
      atacar();
    } else {
      girar(true, false);
    }
  }
  // Mode manual
  if (Serial.available() > 0) {
    byte cAccio = Serial.read();
    switch (cAccio) {
      case ATACAR:
        automatic = false;
        accio = ATACAR;
        atacar();
        break;
      case ATURAR:
        automatic = false;
        accio = ATURAR;
        aturar();
        break;
      case ENRERE:
        automatic = false;
        accio = ENRERE;
        enrere();
        break;
      case GIRAR:
        automatic = false;
        accio = GIRAR;
        girar(false, false);
        break;
      case GIRAR_R:
        automatic = false;
        accio = GIRAR_R;
        girar(true, false);
        break;
      case GIRAR_I:
        automatic = false;
        accio = GIRAR_I;
        girar(false, true);
        break;
      case GIRAR_IR:
        automatic = false;
        accio = GIRAR_IR;
        girar(true, true);
        break;
      case PUJAR_RAMPA:
        automatic = false;
        rampaPujada = true;
        setServo(rampaAlta);
        break;
      case BAIXAR_RAMPA:
        automatic = false;
        rampaPujada = false;
        setServo(rampaBaixa);
        break;
      case AUTOMATIC:
        automatic = true;
        iniciat = false;
        accio = AUTOMATIC;
        break;
      case LEDS_ON:
        ledsOn = true;
        break;
      case LEDS_OFF:
        ledsOn = false;
        break;
      case LEDS_AUTO:
        ledsOn = !ledsOn;
        break;
    }
    chLeds();
  }
#ifdef SERIAL_CONTROL
  sendInfoData(analogRead(sharp), cnyD, cnyE, (int) accio,
               mSpeed, estatMotorD, mSpeed, estatMotorE,
               rampaPujada);
  delay(100);
#endif
}
