#pragma once

/* CNY_MAX <valor> | UNDEF
 *  <valor> = Màxima lectura que és considerada com blanc (inclosa).
 *  UNDEF = Utilitzar CNYs digitals.
 */
#define CNY_MAX 300

/* AUTO_DEFAULT {0 | 1}
 *  0 = Manual al iniciar
 *  1 = Automàtic al iniciar
 */
#define AUTO_DEFAULT 1

/* SERIAL_CONTROL
 *  DEFINE = Envia dades per Serial
 *  UNDEF = No ho fa
 */
#define SERIAL_CONTROL
