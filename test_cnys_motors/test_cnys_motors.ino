#include <AFMotor.h>
#include <Servo.h>

const int cnyD = A4;
const int cnyE = A3;
AF_DCMotor motorD(1);
AF_DCMotor motorE(2);
Servo servo;

const int cnyMax = 300;

void setup() {
  motorD.setSpeed(255);
  motorE.setSpeed(255);
  motorD.run(RELEASE);
  motorE.run(RELEASE);
  servo.attach(10);
}

void loop() {
  servo.write(175); // Rampa baixa
  if ((analogRead(cnyD) <= cnyMax) ||
      (analogRead(cnyE) <= cnyMax)) {
    motorD.run(BACKWARD);
    motorE.run(BACKWARD);
    delay(500);
  } else {
    motorD.run(FORWARD);
    motorE.run(FORWARD);
  }
}
